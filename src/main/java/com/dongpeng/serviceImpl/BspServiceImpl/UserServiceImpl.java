package com.dongpeng.serviceImpl.BspServiceImpl;

import com.dongpeng.dao.BspMapper.UserMapper;
import com.dongpeng.po.BspPo.User;
import com.dongpeng.service.BspService.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by:xiaoqingwen   on 2018/4/10 on 16:52
 */
@Service
public class UserServiceImpl implements UserService{

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    UserMapper userMapper;

    @Override
    public User selectByPrimaryKey(Long id) {
        User user = userMapper.selectByPrimaryKey(id);
        logger.info("用户名称-->{}",user.getName());
        return user;
    }

    @Transactional
    @Override
    public int insertUser(User user) {
        int i = userMapper.insert(user);
        return i;
    }
}
