package com.dongpeng.service.BspService;

import com.dongpeng.po.BspPo.User;

/**
 * Created by:xiaoqingwen   on 2018/4/10 on 16:50
 */
public interface UserService {

    User selectByPrimaryKey(Long id);
    int insertUser(User user);

}
