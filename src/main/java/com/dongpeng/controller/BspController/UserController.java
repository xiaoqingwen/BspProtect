package com.dongpeng.controller.BspController;

import com.dongpeng.po.BspPo.User;
import com.dongpeng.service.BspService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by:xiaoqingwen   on 2018/4/10 on 16:53
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping(value = "selectUser", method = RequestMethod.GET)
    public User selectUser(Long id) {
        User user = userService.selectByPrimaryKey(id);
        return user;
    }

    @ResponseBody
    @RequestMapping(value = "insertUser", method = RequestMethod.GET)
    public String insertUser(){
        User user = new User();
        user.setName("测试001");
        user.setAge(19);
        int i=userService.insertUser(user);
        if(i==1){
            return "添加数据成功!";
        }
        return "添加数据失败!";
    }
}
